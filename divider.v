module divider(q, r, a, b);
    output signed [8:0] q;
    output signed [7:0] r;
    input signed [7:0] a;
	input signed [7:0] b;
	
	reg signed [8:0] posa;
	reg signed [8:0] posb;
	
	reg [16:0] longa;
	reg [16:0] longb;
	
	reg [5:0] bitcnt = 7;
	
	reg signed [0:8] tresq = 9'b0;
	reg signed [0:8] resq = 9'b0;
	reg signed [7:0] resr = 8'b0;
	
	always@(*) begin
		// convert inputs to their absolute value/module to make them positive
		// since it is easier to divide two posivite nubmers
		posa = a < 0 ? (a == -128 ? 128 : {1'b0, -a}) : {1'b0, a};
		posb = b < 0 ? (b == -128 ? 128 : {1'b0, -b}) : {1'b0, b};
		
		// padd the imputs with zeroes so thei can be shifted to the left by 8 positions
		longa = {8'b0, posa};
		longb = {8'b0, posb};
		
		// shift b by 8 positions and check if it fits in a or not
		bitcnt = 8;
		// if it fits in a, mark this in the result with a 1, else mark it with a 0
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		// if it fits, substract it from a and keep the temporary result
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		// apply the algorithm for 7 and less position shift
		bitcnt = 7;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 6;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 5;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 4;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 3;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 2;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 1;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		bitcnt = 0;
		tresq[8 - bitcnt] = (longb << bitcnt) <= longa ? 1'b1 : 1'b0;
		longa = (longb << bitcnt) <= longa ? longa - (longb << bitcnt) : longa;
		
		// if a is negative, then the reminder is negative too, else it is positive
		resr = a < 0 ? -longa : longa;
		
		// if a and b have different signs, the quotient is negative, else it is positive
		resq = (a < 0 && b > 0) || (a > 0 && b < 0) ? -tresq : tresq;
	end
	
	// assign the computed values for the quotient and reminder
	assign q = resq;
	assign r = resr;
endmodule