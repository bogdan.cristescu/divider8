Tema 1 - Împărțire cu semn

Cristescu Bogdan Ion - 334AC

Modulul este descris în reprezentare comportamentală.
Acesta aplică algoritmul de "long division" (algoritmul de calcul manual al împărțirii învatat în
clasele primare) adaptat la numere în baza 2.

Pentru:
deîmpărțit / împărțitor = cât și rest
	<=>
A / B = C rest R
unde A, B, C, R sunt scrise în baza de numerație N cu un număr de P+1 cifre.

Inițial modulul calculează valorile absolute ale A și B pentru a folosi numere pozitive.
Analog celui folosit pentru numerele scrise în baza 10, algoritmul calculează cifra de la poziția
P (numerotată începând cu 0 de la cea mai puțin semnificativă poziție) din rezultat înmulțind B cu
N^P (în esență mutând, în zecimal, binar, și în orice altă bază, B spre stânga și adăugând zerouri
la final) obținând numărul B<<P (B mutat la stânga cu P poziții) și întrebându-se de câte ori
numărul nou format încape în A. Acest rezultat reprezintă cea mai mare putere a bazei N mai mică
decât A.
Algoritmul scade apoi din A pe B<<P înmulțit cu cifra determinată (1 sau 0 în cazul de față) și
obține un rest temporar Rt(P), iar apoi reia calculul pentru cifra următoare folosind B<<P-1,
aflând, deci, cea mai mare putere a bazei N care încape în Rt(P).
Algoritmul se continuă până se determină cifra cea mai puțin semnificativă a lui C iar Rt(P-P)
(Rt(0)) reprezintă restul R al împărțirii.
La final, modulul determină semnele câtului (negativ daca A și B au semne diferite) și restului
(negativ dacă deîmpărțitul este negativ).